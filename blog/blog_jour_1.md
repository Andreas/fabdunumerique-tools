﻿Projet de création d'un blog
================

## Programme de tronc commun en développement

### La fabrique du Numérique

## Consignes

Les exercices qui suivent ainsi que le projet sont à réaliser en binôme. Chacun devra participer, le rôle du binôme est de s'assurer que son partenaire arrive aussi à faire les exercices et de l'aider si ce n'est pas le cas.

Le projet devra aussi être réalisé en double. Toutefois, les choix esthétiques et techniques seront pris à deux.

## Éditeur de texte

Pour réaliser le projet, vous allez avoir à créer des fichiers et écrire du code dedans. Pour cela, vous devez utiliser ce qu'on appel un _éditeur de texte_ qui diffère d'un _traitement de texte_ (comme LibreOffice ou Office).

N'importe quel éditeur suffira pour cet exercice. Vous pouvez utiliser Mousepad, Atom, Gedit, SublimeText, vim, emacs, nano... peu importe tant que ça vous convient.

**Note importante : les systèmes Posix (comme votre Xubuntu ou MacOs) font une différence entre les majuscules et les minuscules dans les noms de fichiers et de dossiers.
C'est assez déroutant quand on vient de Windows mais on s'y fait vite. Faites bien attention à cela quand vous nommez vos fichiers.**

**Note importante 2 : n'essayez pas de tenter de convertir un fichier `.odt` (LibreOffice) en `.txt` ou `.php` ça ne marchera pas ! Le format `odt` étant un format binaire, vous serez surpris de voir tout ce qu'il y a dedans...**

## Projet

Comme je vous l'ai déjà annoncé, pendant 2 jours (étalées sur 2 semaines) vous allez créer un petit blog en Php.

Par blog, j'entends un site internet sur lequel on peut écrire des "articles", les modifier, les supprimer et bien sur les consulter sur la page d'accueil.

Il ne s'agit pas de refaire WordPress en 2 jours, on va se concentrer sur des fonctions d'édition et d'administrations minimales, mais cela vous donnera une idée précise de ce que représente la création d'un site internet où l'on gère du contenu.

## Serveur web et PHP

Ce projet étant à caractère pédagogique, nous n'allons pas configurer un vrai serveur web de type Apache ou Ngnix mais utilisé le serveur web de développement fournis par l'interpréteur Php.


### Késako ?

#### Serveur Web

Un serveur web est un logiciel permettant d’exécuter des requêtes HTTP, le fameux protocole que nos navigateurs web adorés utilisent.

Par défaut, un serveur web ne peut servir que du contenu statique, c’est-à-dire des pages HTML, du texte, des fichiers son, vidéos, des images, etc.

Vous pouvez tester cette fonctionnalité en créant un dossier dans votre espace personnel, puis en rajoutant les fichiers que vous voulez dedans.

_Note : Nous utiliserons toujours le même repertoire tout au long de l'exercice. _

Ensuite, ouvrez un terminal (programme appelé "Émulateur de terminal" dans Xubuntu) et allez dans ce répertoire en utilisant la commande

`cd nomdevotredossier` pour "change directory".

Pour vous assurer que vous êtes bien dans le bon répertoire, vous pouvez utiliser la commande `pwd` qui affiche le répertoire courant.

Vous pouvez aussi lister le contenu du répertoire via la commande `ls`.

Pour lancer un serveur web à partir de ce répertoire utiliser la commande suivante (le numéro de port est arbitraire tant qu'il est supérieur à 1024 c'est bon) : 

`php -S localhost:1337`

**Note : cette commande lance un serveur web dans votre terminal quoi doit resté ouvert tout le long de l'exercice. Vous allez voir que des logs s'afficheront quand vous accéderez à votre page web.**

_Note 2 : si php n'est pas installé, vous pouvez tapper `sudo apt install php`_

Vous pouvez maintenant lancer des requêtes Http vers cette adresse et avoir une réponse.

Bien sûr, vous pouvez passer par votre navigateur en utilisant l'adresse `http://localhost:1337`, mais vous  pouvez aussi utiliser des outils en ligne de commande tel que `curl` qui permette de faire une requête http manuellement.

Par exemple, créez un fichier `fabrique.txt` avec le texte que vous voulez dans votre répertoire.

Ensuite, **dans une nouvelle fenêtre de terminal (ctrl+shift+t)** tapez : 

`curl localhost:1337/fabrique.txt`

_Note : si curl n'est pas installé, vous pouvez tapper `sudo apt install curl`_

Vous devriez voir apparaître le contenu de votre fichier dans le terminal.

Votre navigateur fait pareil pour vous afficher le contenu d'un site.

Essayez les différents fichiers de votre dossier dans votre navigateur et regarder ce que ça fait.

**Petit tips sympa : dans votre terminal, utiliser la flèche du haut pour naviguer dans votre historique de commande et la touche `tab` pour avoir une _autocomplétion_ de vos chemin avec `cd`**

#### Php

Servir du contenu statique, c'était bien dans les années 90, maintenant, tout le monde veut pouvoir partager sa vie sur internet sans avoir à apprendre comment écrire un site web, ni comment envoyer un fichier dessus via FTP. Sans parler des problèmes de sécurité que pourrait causer le fait de laisser à tout le monde
l'accès direct à un serveur.

Si vous souhaitez avoir du contenu dynamique éditable par l'utilisateur, comme le contenu de notre blog ou une page Wikipédia par exemple, il vous faudra utiliser un _langage de programmation_, comme Php que nous allons découvrir.

Je sais, ce mot peut faire peur, ça paraît obscur, réservé à une élite de mecs super doués en mathématiques, mais vous allez voir que tout ça est très logique et pas réservé aux personnes qui aiment les maths. J'ai jamais été très bon en maths et pourtant je fais de la programmation depuis plus de 10 ans.

Il faut voir Php comme un outil qui vous permettra d'automatiser certaines tâches pour votre site web.

Sans lui, pour écrire un blog, vous devrez manuellement écrire vos articles HTML, les envoyer en Ftp sur un serveur web sans garantie de ne pas faire d'erreur (en Ftp on écrase le fichier précédent) et cette action devra être répétée à chaque fois que vous voulez rajouter du contenu.

Pire, si vous voulez qu'une autre personne puisse écrire sur votre blog, il faudra lui donner les accès Ftp à votre serveur (ou qu'elle passe par vous à chaque fois), lui apprendre HTML, etc. bref, on voit bien que cela devient quasiment impossible de collaborer efficacement.

Dans ce projet, nous allons utiliser Php pour lire des articles contenus dans un fichier, les afficher et permettre leur création/édition ainsi que leur suppression.

Tout ça, sans que la personne qui utilisera notre site web ait des connaissances en programmation, ni un accès direct au serveur web. Elle est pas belle la vie !

La commande `php -S` permet donc de lancer un serveur web directement relié à l'interpréteur php.

Cela veut dire que si je mets un fichier php dans le répertoire du serveur, il ne sera pas renvoyé comme un fichier normal (par exemple un fichier texte) mais sera _interprété_ par php avant.

##### Exercice : à la découverte de l'interpréteur Php

Un petit test s'impose !

Créer un fichier `coucou.txt` avec le contenu suivant. Le `\n` étant pour faire un retour à la ligne :

```php
<?php
echo "Coucou\n";
```

Faites maintenant la requête suivante : 

`curl localhost:1337/coucou.txt`

Que se passe-t-il ?

Maintenant, renommer votre fichier (avec la commande `mv anciennom nouveaunom` par exemple) en `coucou.php`.

Fait la requête : 

`curl localhost:1337/coucou.php`

Qu'est ce qui a changé ?

Le fichier est passé par l'interpréteur php qui a reconnu la commande `echo` comme étant une commande d'affichage.

**Note importante : il se peut que votre script Php ne renvoit rien et n'affiche rien, c'est souvent le cas quand il y a une erreur dans votre code. Regarder dans les logs de votre serveur web pour voir ce qu'il se passe.**

##### Exercice : les variables

Un truc très pratique dans un langage de programmation est la notion de _variables_, sorte d'étiquette que l'on met au contenu que l'on veut.

Reprenons notre fichier `coucou.php` et modifions le un peu :

```php
<?php
$name = "Johnny";
echo "Coucou $name\n";
```

Ce code signifie :

* Met dans une variable nommée `$name` (le $ devant c'est pour dire à Php que c'est une variable), la valeur "Johnny"
* affiche "coucou " suivi du contenu de la variable `$name` et d'un retour à la ligne (`\n`).

Alors oui, y a un truc chelou au début du fichier `<?php`, des caractères `$`, `\n` bizarres et des `;` à la fin des lignes, c'est pas comme en français.

C'est sûr, les langages de programmation ont une _grammaire_ propre à eux, mais vous allez voir qu'elle est en réalité bien plus simple que nos langages écrits.

Ici pas de "complément d'objet direct", pas de verbe à conjuguer, pas d'accords de pluriel, peu voir pas d'exception à la règle contrairement au français.

Bref, mise à part quelques nouveaux signes de ponctuation comme les `[]` ou les `{}` ça devrait aller !

##### Les variables GET

Le protocole HTTP permet de passer des variables dans les requêtes. La requête principale est la requête `GET` et on rajoute les variables dans l'_url_, je suis sûr que vous avez déjà vu ça quelque part...

Modifiez le fichier `coucou.php` de la sorte :

```php
<?php
$name = $_GET['name'];
echo "Coucou $name\n";
```

Puis faite la requête suivante : 

`curl localhost:1337/coucou.php?name=Hallyday`

Testez avec d'autres valeurs pour `name`.

Alors vous commencez à comprendre l'intérêt d'utiliser un langage de programmation ?

Comment aurions-nous pu faire ça avec des fichiers statiques sans Php ? Et bah on aurait juste pas pu !

## HTML

Je vous en ai déjà parlé, le navigateur web utilise le _langage de balisage_ HTML pour afficher et structurer les données sur une page.

### Exercice : différence entre fichier texte et HTML

Créez un fichier index.txt avec le contenu suivant :

```
Blog de la Fabrique

Bienvenue sur notre blog
```

Allez **avec votre navigateur** à l'adresse suivante : 

`localhost:1337/index.txt`

Pas très sexy non ?

Maintenant modifier votre fichier de la sorte :

```html
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Blog La fabrique</title>
</head>
<body>
  <h1>Blog de la Fabrique</h1>
  <p>
    Bienvenue sur notre blog
  </p>
</body>
</html>
```

Faites F5 dans votre navigateur... encore pire non ?

Maintenant renommer le fichier `index.txt` en `index.html`

Allez maintenant à l'adresse suivante : `localhost:1337` (pas besoin de préciser `index.html`)

C'est déjà mieux non ?!

Le Html permet de _dire_ des choses aux navigateurs qui vont lui permettre de mieux faire son boulot.

Par exemple, avec ce bout de code on a :

* Précisé que le contenu de notre site était en français (`lang="fr"`)
* Précisé que l'on utilisait l'_encodage_ UTF-8 pour nos fichiers. L'encodage est une notion extrêmement importante que l'on a pas trop le temps de voir mais quand vous recevez des messages avec des caractères genre `?` à la place des accents `é` c'est qu'il y a eu un problème d'encodage quelque part. Une seule   chose à retenir : assurez vous que tous vos contenus sont en `utf-8`.
* Précisé un titre pour l'onglet de notre page. Cela permet de pouvoir le retrouver facilement.
* Précisé que l'on a un titre de niveau 1 (`<h1>`)
* Et un paragraphe (`<p>`)

Même pour les non développeurs, il est indispensable d'avoir des bases en Html si vous voulez travailler dans le web. Car, Html ne permet pas de parler qu'à votre navigateur, il _parle_ aussi aux _robots_ des moteurs de recherche qui eux utilisent plutôt des outils comme `curl` qu'un navigateur web.

### Exercice : Html et Php

Php a été créé dans le but de pouvoir dynamiser une page html. C'est pour cela que tout code php doit être entre des balises `<?php ?>` ou simplement `<? ?>`

_Note : si on ne veut faire que du php dans un fichier, il est possible de mettre que la balise ouvrante `<?php` et d'omettre la balise fermante `?>`. C'est même conseillé._

On va donc renommer notre fichier `index.html` en `index.php` et s'assurer que tout fonctionne encore en faisant F5 dans notre navigateur.

Maintenant, on va modifier très légèrement le fichier en remplaçant le paragraphe par :

```php
<p>
  Bonjour <?= $_GET['name'] ?>, bienvenue  sur notre blog.
</p>
```

_Note : l'écriture `<?=` est un raccourci pour l'écriture `<?php echo`, pratique non ?_

Allez maintenant à l'adresse : 

`localhost:1337/?name=Eddy`

Testez plusieurs valeurs pour `name`.

Mais c'est que ça devient sympa tout ça !! :)

## Des articles !

Passons aux choses sérieuses et utilisons Php pour générer un site avec du contenu.

Les sites internet "normaux" utilisent généralement ce que l'on appelle une _base de données_ pour stocker les données des utilisateurs et du site.

C'est une méthode sécurisée et performante qu'il convient d'utiliser quand on cherche à faire un "vrai" site.

Pour des raisons pédagogiques, nous allons stocker nos articles de blogs dans un fichier _csv_. C'est un format de fichier très répandu qui permet de stocker facilement des données _tabulaires_.

Pourquoi CSV ? Parce que l'on peut éditer un fichier CSV dans un outil comme Excel et ça permet de se rendre plus facilement compte de la manière dont un site manipule les données.

Les _systèmes de gestion de bases de données_ tel que _MySQL_ sont certes très répandu mais un peu plus difficiles à utiliser, car ils font appel au langage SQL (encore un nouveau langage).

Je voulais vous épargner son apprentissage pour ces deux jours déjà bien denses...

### Exercice : Lecture d'un CSV

Le fichier [`articles.csv`](https://framagit.org/Andreas/fabdunumerique-tools/raw/master/blog/articles.csv) (sauvegardez le en faisant `ctrl+s`) contient plusieurs articles avec un titre (nommé `title` car en programmation tout est en anglais), un contenu (`content`) et un auteur (`author`).

_Note: le contenu de ce fichier provient de l'excellent [framablog](https://framablog.org/), merci à eux au passage !_

Vous pouvez ouvrir ce fichier avec LibreOffice Calc, ajouter des articles, les modifier, en supprimer, etc. le format CSV est assez pratique.

Ça sera pour l'instant notre "panel d'admin du pauvre" car on a pas encore codé l'interface d'admin de notre site en php.

Le but de cet exercice est de lire le fichier `articles.csv` dans un programme php et d'afficher les articles sur notre page d'accueil.

_Note : le [format csv](https://fr.wikipedia.org/wiki/Comma-separated_values) permet de stocker de manière simple des données en ligne et colonne (comme ce que vous pouvez faire avec un tableau type Excel ou Calc).
Vous pouvez ouvrir les fichiers `.csv` dans LibreOffice Calc ou dans Office, la différence avec les formats comme `xls`, `xlsx` ou `ods` est que `csv` est un format textuel et donc facilement lisible par une application tierce comme notre site web._

Pour vous aider, je vous ai codé une petite fonction php "vite fait mal fait" (à ne pas réutiliser sur d'autres projets donc) qui simplifie la lecture et l'utilisation de fichier csv.

Voici son contenu que vous pouvez mettre dans un fichier `csv-utils.php` par exemple :

_Note : pas besoin de comprendre ce qui suit, vous n'avez qu'à copier-coller._

```php
<?php

function parseCsv($fileName) {
  $content = [];
  $handle = fopen($fileName, 'r');
  if ($handle !== FALSE) {
    $keys = fgetcsv($handle);
    $numCol = count($keys);

    while (($data = fgetcsv($handle)) !== FALSE) {
      $row = [];
      for ($c=0; $c < $numCol; $c++) {
        $row[$keys[$c]] = $data[$c];
      }
      $content[] = $row;
    }
    fclose($handle);
  }
  return $content;
}
```

Ensuite, on va modifier notre fichier `index.php` afin qu'il puisse utiliser cette fonction.

Pour cela, ajoutez au tout début du fichier l'instruction suivante :

```php
<?php
require('csv-utils.php');

$articles = parseCsv('articles.csv');
?>
```

Le `require` va nous permettre d'utiliser la fonction `parseCsv` présente dans le fichier `csv-utils.php`.

Une fonction est une portion de code réutilisable. Sans rentrer dans les détails, je vais pouvoir utiliser `parseCsv` avec plusieurs fichiers CSV si je veux.

Exemple :

```php
$articles = parseCsv('articles.csv');
$trucs = parseCsv('truc.csv');
```

Grosso modo, ça évite d'avoir à écrire plusieurs fois le même code. C'est super pratique et on en utilise tout le temps en programmation.

Mais, reprenons notre fil.

Grâce à la fonction `parseCsv`, on a maintenant le contenu de notre fichier csv dans une variable nommée `$articles`.

Il nous reste plus qu'à les afficher !

Modifier le fichier `index.php` en rajoutant un nouveau paraphe à la suite de celui (après le `</p>`, avant le `</body>`) qu'on a déjà mis :

```php
  <h2><?= $articles[0]['title'] ?></h2>
  <small>par <?= $articles[0]['author'] ?></small>
  <p>
    <?= $articles[0]['content'] ?>
  </p>
```

Testez dans votre navigateur, pas mal non ?

_Note : si rien ne s'affiche, assurez-vous que le fichier [`articles.csv`](https://framagit.org/Andreas/fabdunumerique-tools/raw/master/blog/articles.csv) est bien présent dans votre dossier._

Là, on affiche que le premier article de la liste (oui les tableaux en Php commence par 0, perturbant non ?).

Comment faire en sorte que tous les articles, peu importe leur nombre, puissent s'afficher ?

Grâce à l'instruction [`foreach`](http://php.net/manual/fr/control-structures.foreach.php) !

Voici le code pour afficher tous les titres que vous pouvez placer avant le `</body>` :

```php
<?php
foreach($articles as $art) {
  echo "<h2>" . $art['title'] . "</h2>";
}
?>
```

Maintenant, à vous de le modifier pour aussi afficher l'auteur (dans une balise `<small>`) et le contenu (dans une balise `<p>`).

Ouvrez le fichier `article.csv` avec LibreOffice Calc, rajoutez des articles, sauvegardez. Rafraîchissez votre page, est-ce que les nouveaux articles apparaissent ?

Si oui, vous pouvez passer à la suite :).

## De _jolis_ articles grâce au CSS

On est arrivé à afficher le contenu de notre fichier csv sous forme d'un site web accessible depuis un navigateur, c'est super, mais avouez que c'est tout de même un peu austère ce noir sur blanc.

Grâce au langage CSS, vous allez pouvoir modifier à volonterl'affichage de votre page web !

Pour voir la puissance du CSS, je vous invite à visiter le site
[css zen garden](http://www.csszengarden.com/) où [la même page HTML](http://www.csszengarden.com/examples/index) est mise en page de [millier](http://www.csszengarden.com/221/) [de](http://www.csszengarden.com/222/) [manières](http://www.csszengarden.com/223) [différentes](http://www.csszengarden.com/220/) juste avec du CSS.

### Ajout d'un fichier CSS et première classe

Nous allons donc ajouter un fichier css à notre page.

Créez un fichier `styles.css` et référencez-le dans votre page en rajoutant cette ligne juste après la balise `<meta>` présente dans le fichier `index.php` :

```html
<link rel="stylesheet" href="styles.css">
```

En css, on peut soit modifier directement la manière dont _tous_ les tags (par exemple les `h2` de la page vont être rendus) ou seulement ceux que l'on voudra en utilisant des _classes_.

Ajouter dans le fichier `styles.css` la directive suivante :

```css
h2 {
  color: red;
}
```

Je sais ça pique les yeux, j'ai jamais été bon designer...

Si vous voulez limiter cette directive qu'à certain éléments, vous pouvez utiliser une _classe_ CSS qui se définit avec un `.`.

Modifier le fichier `styles.css` de la sorte :

```css
.rouge {
  color: red;
}
```

Maintenant, seul les éléments html avec comme _attribut_ `class="rouge"` seront concernés.

Exemple :

```html
<h2 class="rouge">Mon titre en rouge</h2>
<p class="rouge">Les classes s'appliquent a tous les éléments html</p>
```

### Exercice : quartier libre !

Après cette brève et incomplète introduction du CSS, je vous invite à faire joujou avec en vous inspirant de site que vous trouverez sur internet pour créer le design que vous souhaitez pour votre site web.

Vous pouvez aussi rajouter des images et toutes sortes d'éléments, vous êtes libre de faire ce qu'il vous plaît !

_Note : les outils de développeur de votre navigateur vont vous êtes d'une grande aide. `ctrl + shift + i` ou `F12`_

#### Quelques sites pour vous aider

Afin d'apprendre un peu plus en détail l'html et le css (ou même le Php), je vous conseille cet excellent site [codecademy](https://www.codecademy.com/).

Il existe [un parcours en français pour l'html et le css](https://www.codecademy.com/fr/tracks/html-css-french) mais il est un peu buggé, ça peut toutefois convenir si vous êtes allergique à l'anglais.

Sinon, il y a aussi [openclassrooms](https://openclassrooms.com/) qui propose des parcours de qualité.

Si vous avez besoin d'avoir des références vers les différentes balises html, [ce site](https://github.com/gendx/html-cheat-sheet) peut vous être utile.

Ensuite, il y a énormément de ressource sur internet pour apprendre Html, Css et Php, je vous laisse découvrir par vous même :).

Amusez-vous bien !

## Accéder au blog des copains !

Vous pouvez accéder au blog de vos camarade grâce à leur adresse IP.

Comment faire ?

Tout d'abord il faut récupérer son adresse IP via la commande `ifconfig`, vous devriez avoir quelque chose comme :
```
wlan1     Link encap:Ethernet  HWaddr 78:92:9c:0f:a6:0a  
          inet adr:10.20.0.54  Bcast:10.20.0.255  Masque:255.255.255.0
          adr inet6: fe80::7a92:9cff:fe0f:a60a/64 Scope:Lien
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          Packets reçus:46050 erreurs:0 :0 overruns:0 frame:0
          TX packets:41525 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 lg file transmission:1000 
          Octets reçus:23305069 (23.3 MB) Octets transmis:8594404 (8.5 MB)
```

Votre adresse Ip se trouve juste après `inet adr:`.

Maintenant, il suffit de relancer votre serveur avec votre Ip à la place de `localhost`.

Exemple :

`php -S 10.20.0.54:1337`

Maintenant toutes les personnes dans le réseau local pourront accéder à votre site web via un navigateur. La classe non ?

Exemple : avec mon Ip, vous pouvez vous rendre sur `10.20.0.54:1337` et tomber sur mon site si mon serveur est actif.

## Ajouter des images

Il serait sympa de pouvoir définir dans notre fichier csv une image qui illustrera notre article de blog.
Celle-ci pourra se trouver par exemple en dessous de notre titre.

Rajoutez donc une colonne `image` dans le fichier `articles.csv` et vous mettrez le nom d'une image qui se trouve par exemple dans un dossier `img` présent dans le dossier de votre projet.

Ensuite, dans votre fichier `index.php`, rajoutez une balise `<img>` sous votre titre dont l'_url_ (attribut `src`) sera celle contenue dans la colonne `image` du csv.

Exemple de balise img :
```html
<img src="img/nomimage.jpg">
```


## Ne pas afficher les messages trop longs

Certains articles peuvent être très long et il serait super qu'à partir d'une certaine taille,
 ils soient tronqués avec une petite phrase genre _"Lire la suite..."_ qui est un lien (balise `<a>`) pour ouvrir l'article en entier.

### Déterminer la taille de l'article

Pour connaître la taille du contenu, vous pouvez utiliser la fonction [strlen](http://php.net/manual/fr/function.strlen.php) de php sur le contenu.

```php
strlen($art['content'])
```

Ensuite il vous faut tester si cette taille n'est pas trop grande.

Pour faire des tests, il existe un mot clé essentiel dans les langages de programmation : le [`if`](http://php.net/manual/fr/control-structures.if.php).
`if` nous permet d'executer du code uniquement s'il valide une condition. Si vous voulez faire quelque chose quand la condition n'est pas validée, vous pouvez utiliser [`else`](http://php.net/manual/fr/control-structures.else.php).

Exemple d'utilisation :

```php
// Tout ce qui est après les "//" sont des commentaires
// On teste si la taille est supérieure à 200 caractères
if(strlen($content) > 200) {
    // Afficher les 200 premiers caractères et le lien pour lire la suite
}
else {
    // Afficher le contenu en entier
}
```

Pour n'afficher qu'une partie du contenu, vous pouvez utiliser la fonction [`substr`](https://secure.php.net/manual/fr/function.substr.php) de la sorte :

```php
// Prend les 200 premier caractères d'une chaine
$truncatedContent = substr($content, 0, 200);
echo $truncatedContent;
```

_Note : Ce code est à mettre dans le fichier `index.php` à l'endroit où vous affichez les articles._

Pour afficher l'article en totalité dans une page à part. On va créer un fichier `article.php` auquel on passera l'id en paramètre GET.

On aura ainsi dans notre fichier `index.php` des liens de type `<a href="article.php?id=1">Lire la suite</a>`.


Pour pouvoir récupérer un article par son `id`, voici une nouvelle fonction que vous pouvez rajouter à la suite de votre fichier `csv-utils.php` :

```php
function getByIdCsv($fileName, $id) {
  $content = parseCsv($fileName);
  $key = array_search($id, array_column($content, 'id'));
  if($key !== FALSE) {
    return $content[$key];
  }
  return FALSE;
}
```

Ensuite il ne vous reste plus qu'a utiliser cette fonction dans votre fichier `article.php` en utilisant la variable `$_GET` pour retrouver l'id.

```php
<?php
require('csv-utils.php');

$article = getByIdCsv('articles.csv', $_GET['id']);
```

Et afficher une page html avec le contenu de la variable `$article`.

## Création d'article

Il serait cool de pouvoir créer un article directement depuis l'interface de notre site web sans avoir à modifier manuellement le fichier csv.

Pour ça, vous pouvez vous aider de cette nouvelle fonction qui vous permettra de rajouter une nouvelle ligne dans le fichier csv.

La fonction `getNextIdCsv` est nécessaire pour rajouter automatiquement le champs `id` en fonction de l'id le plus grand présent dans le csv.

```php
/* Note : first column must be id */
function getNextIdCsv($fileName) {
  $nextId = -1;
  $handle = fopen($fileName, 'r');
  if ($handle !== FALSE) {
    $keys = fgetcsv($handle);
    if($keys[0] === 'id') {
      while (($data = fgetcsv($handle)) !== FALSE) {
        if($data[0] >= $nextId) {
          $nextId = $data[0] + 1;
        }
      }
    }
  }
  return $nextId;
}

function addToCsv($fileName, $row) {
  $id = getNextIdCsv($fileName);
  $handle = fopen($fileName, 'r');
  if ($handle !== FALSE) {
    $keys = fgetcsv($handle);
    fclose($handle);
  }
  $handle = fopen($fileName, 'a');
  if ($handle !== FALSE && $id !== -1) {
    $row['id'] = $id;
    $csvRow = [];
    foreach ($keys as $index => $key) {
      $csvRow[$index] = $row[$key];
    }
    fputcsv($handle, $csvRow);
    fclose($handle);
  }
}
```

Vous n'avez plus qu'à créer une page `create_article.php` avec un formulaire, 2 champs `<input>` type `text` pour le titre et l'auteur et un champs `<textarea>` pour le contenu.

Ajoutez aussi un `<input>` type `submit` pour avoir un bouton de création.

La `method` du formulaire doit être `POST` afin de pouvoir savoir si vous êtes en train d'afficher le formulaire ou justement de le remplir.

Vous pouvez décrire votre formulaire de cette manière :

```html
<form action="create_article.php" method="POST">
    <!-- Contenu de votre formulaire -->
</form>
```

Comme je suis sympa, je vous file le code de la page : 
```html
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="styles.css">
  <title>Ajout d'article</title>
</head>
<body>
  <h1>Création d'un article</h1>
  <form action="create_article.php" method="POST">
    <div>
      <label for="title">Titre</label>
      <input type="text" name="title">
    </div>
    <div>
      <label for="author">Auteur</label>
      <input type="text" name="author">
    </div>
    <div>
      <label for="content">Contenu</label>
      <textarea name="content"></textarea>
    </div>
    <input type="submit" value="Créer">
  </form>
</body>
</html>
```


Pour testé que vous êtes sur vous avez envoyé votre formulaire (via la méthode `POST` donc), vous pouvez faire cela :

```php
if($_POST) {
    // Je créé mon article ici avec les données présentent dans la variables $_POST
}
```

Une fois l'article créer vous pouvez renvoyer à la page d'accueil via ce code :

```php
header('Location: /');
```

## Modification et suppression dans le CSV

Voici la version complète de notre "librairie" très vite fait et très mal fait pour éditer des CSV.

Vous pouvez remplacer le contenu du fichier `csv-utils.php` par celui-ci :

```php
<?php

function parseCsv($fileName) {
  $content = [];
  $handle = fopen($fileName, 'r');
  if ($handle !== FALSE) {
    $keys = fgetcsv($handle);
    $numCol = count($keys);
    // First column must be id
    if($keys[0] !== 'id') {
      echo "Erreur dans le fichier $fileName, la première colonne n'est pas 'id'";
      return FALSE;
    }

    while (($data = fgetcsv($handle)) !== FALSE) {
      $row = [];
      for ($c=0; $c < $numCol; $c++) {
        if($c === 0 && $data[$c] === "" || is_integer((int)$data[$c]) === FALSE) {
          echo "<br>Erreur dans le fichier $fileName, 'id' invalide sur la ligne";
          print_r($data);
        }
        $row[$keys[$c]] = $data[$c];
      }
      $content[] = $row;
    }
    fclose($handle);
  }
  return $content;
}

function getCsvContentKey($content, $id) {
  return array_search($id, array_column($content, 'id'));
}

function getByIdCsv($fileName, $id) {
  $content = parseCsv($fileName);
  $key = getCsvContentKey($content, $id);
  if($key !== FALSE) {
    return $content[$key];
  }
  return FALSE;
}

/* Note : first column must be id */
function getNextIdCsv($fileName) {
  $nextId = -1;
  $handle = fopen($fileName, 'r');
  if ($handle !== FALSE) {
    $keys = fgetcsv($handle);
    if($keys[0] === 'id') {
      while (($data = fgetcsv($handle)) !== FALSE) {
        if($data[0] >= $nextId) {
          $nextId = $data[0] + 1;
        }
      }
    }
  }
  return $nextId;
}

function addToCsv($fileName, $row) {
  $id = getNextIdCsv($fileName);
  $handle = fopen($fileName, 'r');
  if ($handle !== FALSE) {
    $keys = fgetcsv($handle);
    fclose($handle);
  }
  $handle = fopen($fileName, 'a');
  if ($handle !== FALSE && $id !== -1) {
    $row['id'] = $id;
    $csvRow = [];
    foreach ($keys as $index => $key) {
      $csvRow[$index] = $row[$key];
    }
    fputcsv($handle, $csvRow);
    fclose($handle);
    return TRUE;
  }
  return FALSE;
}

function writeContentToCsv($fileName, $content) {
  $handle = fopen($fileName, 'w');
  if($handle !== FALSE && count($content) > 0) {
    fputcsv($handle, array_keys($content[0]));
    foreach ($content as $row) {
      fputcsv($handle, array_values($row));
    }
    fclose($handle);
    return TRUE;
  }
  return FALSE;
}

function removeFromCsv($fileName, $id) {
  $content = parseCsv($fileName);
  $key = getCsvContentKey($content, $id);
  if($key !== FALSE) {
    array_splice($content, $key, 1);
    return writeContentToCsv($fileName, $content);
  }
  return FALSE;
}

function editCsvRow($fileName, $row) {
  $content = parseCsv($fileName);
  $key = getCsvContentKey($content, $row['id']);
  if($key !== FALSE) {
    $content[$key] = $row;
    return writeContentToCsv($fileName, $content);
  }
  return FALSE;
}
```

## Panel Admin

C'est bien de pouvoir créer de nouveaux articles depuis le site web sans passer
par le CSV. Il serait encore mieux de pouvoir gérer tous nos articles depuis une
interface d'administration.

Vous êtes partant pour faire ça ?

Voici le code du panel d'admin :

```html
<?php
require('csv-utils.php');

$mesArticles = parseCsv('articles.csv');

?>
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Admin - Blog</title>
</head>
<body>
<h1>Administration des articles</h1>
  <a href="create_article.php"><button>Nouvel article</button></a>
  <ul>
  <?php foreach ($mesArticles as $art): ?>
    <li>
      <?= $art['title'] ?>
      <a href="edit_article.php?id=<?= $art['id'] ?>"><button>Editer</button></a>
      <a href="delete_article.php?id=<?= $art['id'] ?>"><button>Supprimer</button></a>
    </li>
  <?php endforeach; ?>
  </ul>
</body>
</html>
```

### Lister tous les articles

On va commencer par créer un fichier `admin.php` qui listera tous les titres des
articles.

Vous avez déjà fait ça :

* Faire un `require` du fichier `csv-util.php`
* Appeler la fonction `parseCsv` sur le fichier `articles.csv`
* Créer une page Html complète avec un `<body>`
* Faire un `foreach` sur tous les articles
* Pour chaque article on affichera le titre, non pas dans un `<h2>` cette fois
  mais plutôt dans un `<li>` qui devra être entouré d'un `<ul>`.

Je vous laisse vous débrouiller avec ces explications

### Ajout d'un bouton de suppression

Ensuite, vous devrez ajouter un bouton de suppression pour chaque article qui appelera un script php qui supprimera l'article avec l'id en question dans le CSV.

### Ajout d'un bouton d'édition

Ajoutez maintenant un bouton d'édition pour chaque article que vous affichera le même formulaire que pour la création d'article mais comprenant le contenu de l'article et modifiant la ligne du CSV.

## Ajout d'un bouton création

Ajouter un lien pour pouvoir créer un article depuis le panel d'admin

## Bonus : Ajout d'un système d'identification

En utilisant les sessions php, rajouter une interface d'authentification pour faire panel d'admin.

## Bonus : éditeur WYSIWYG d'article

Modifier la page de création et d'édition d'article pour qu'on puisse faire de la mise en page avec un éditeur de type WYSIWYG.

Vous pouvez utiliser [ce superbe éditeur](http://wysihtml.com/) si vous voulez.