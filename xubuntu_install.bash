echo
echo "=============================="
echo "Removing useless Xubuntu package"
echo "=============================="

sudo apt-get remove -y ristretto parole apport

echo
echo "=============================="
echo "Adding Peek, shotcut, Atom and nodejs ppa"
echo "=============================="

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo add-apt-repository ppa:haraldhv/shotcut
sudo add-apt-repository ppa:peek-developers/stable
sudo add-apt-repository ppa:webupd8team/atom

echo
echo "=============================="
echo "Updating package list and upgrading system"
echo "=============================="
sudo apt-get update
sudo apt-get upgrade -y

echo
echo "=============================="
echo "Installing good multimedia packages"
echo "=============================="
sudo apt-get install -y vlc xubuntu-restricted-extras libavcodec-extra cheese shotwell shotcut libdvd-pkg
#Install encrypted dvd library
sudo dpkg-reconfigure libdvd-pkg

echo
echo "=============================="
echo "Installing graphic design packages"
echo "=============================="
sudo apt-get install -y gimp inkscape pinta peek

echo
echo "=============================="
echo "Installing system packages"
echo "=============================="
sudo apt-get install -y pdftk gnome-system-monitor synaptic gdebi-core gksu rar git curl unrar usb-creator-gtk wget gparted gnome-font-viewer


echo
echo "=============================="
echo "Installing web packages"
echo "=============================="
sudo apt-get install -y chromium-browser filezilla icedtea-plugin

echo
echo "=============================="
echo "Installing Node.js and Atom"
echo "=============================="
sudo apt-get install -y nodejs atom

echo
echo "=============================="
echo "Installing MySql"
echo "=============================="
sudo apt-get install -y mysql-server libmysqld-dev

echo
echo "=============================="
echo "Installing Php and common modules"
echo "=============================="
sudo apt-get install -y php php-mysql 
